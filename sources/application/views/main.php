<?php $this->load->view('header'); ?>
    <main class="main">
      <section class="slice" style="padding-top: 8rem;">
 
          <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
  <div class="carousel-inner">
    <div class="carousel-item active">
      <div class="item-slider" style="padding: 10% 15%; height: 60vh; background-image: url(<?php echo site_url();?>resources/assets/images/4.jpg); background-size: cover; background-position: center;">
        <h1 style="color: #fff;">Найдите надежного партнера среди профессионалов своего дела.</h1>
        <h3 style="color: #fff;">Составьте заявку по интересуемой вами услуги, и получите компетентную консультацию и сервис.</h3>
        <a href="reg.html"> <button type="button" class="btn btn-primary btn-animated btn-animated-x">
                    <span class="btn-inner--visible">Зарегистрироваться</span>
                    <span class="btn-inner--hidden"><svg class="svg-inline--fa fa-play fa-w-14" aria-hidden="true" data-prefix="fas" data-icon="play" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" data-fa-i2svg=""><path fill="currentColor" d="M424.4 214.7L72.4 6.6C43.8-10.3 0 6.1 0 47.9V464c0 37.5 40.7 60.1 72.4 41.3l352-208c31.4-18.5 31.5-64.1 0-82.6z"></path></svg><!-- <i class="fas fa-play"></i> --></span>
                  </button></a>
      </div>
    </div>
    <div class="carousel-item">
            <div class="item-slider" style="padding: 10%; height: 60vh; background-image: url(<?php echo site_url();?>resources/assets/images/5.jpg); background-size: cover; background-position: center;">
        <h1 style="color: #fff;">Выберите партнера по вашим критериям<br> и получите профессиональную консультацию и сервис.</h1>
         <h3 style="color: #fff;">Составьте заявку по интересуемой вами услуги, и получите компетентную консультацию и сервис.</h3>
        <a href="reg.html"><button type="button" class="btn btn-primary btn-animated btn-animated-x">
                    <span class="btn-inner--visible">Зарегистрироваться</span>
                    <span class="btn-inner--hidden"><svg class="svg-inline--fa fa-play fa-w-14" aria-hidden="true" data-prefix="fas" data-icon="play" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" data-fa-i2svg=""><path fill="currentColor" d="M424.4 214.7L72.4 6.6C43.8-10.3 0 6.1 0 47.9V464c0 37.5 40.7 60.1 72.4 41.3l352-208c31.4-18.5 31.5-64.1 0-82.6z"></path></svg><!-- <i class="fas fa-play"></i> --></span>
                  </button></a>
      </div>
    </div>
  </div>
  <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>

      </section>
    </main>
	<?php $this->load->view('footer'); ?>