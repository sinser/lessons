<?php $this->load->view('header'); ?>
    <main class="main">
      <section class="py-xl bg-cover bg-size--cover" style="background-image: url('<?php echo site_url();?>resources/assets/images/backgrounds/img-1.jpg')">
        <span class="mask bg-primary alpha-6"></span>
        <div class="container d-flex align-items-center no-padding">
          <div class="col">
            <div class="row justify-content-center">
              <div class="col-lg-4">
                <div class="card bg-primary text-white">
                  <div class="card-body">
                    <button type="button" class="btn btn-primary btn-nobg btn-zoom--hover mb-5">
                      <a style="color: #fff; text-decoration: none;" href="main.html"><span class="btn-inner--icon"><i class="fas fa-arrow-left"></i></span></a>
                    </button>
                    <span class="clearfix"></span>
                    <img src="<?php echo site_url();?>resources/assets/images/brand/icon.png" style="width: 50px;">
                    <h4 class="heading h3 text-white pt-3 pb-5">Добро пожаловать,<br>
                      войдите в ваш личный кабинет.</h4>
                    <form class="form-primary">
                      <div class="form-group">
                        <input type="email" class="form-control" id="input_email" placeholder="Ваш email">
                      </div>
                      <div class="form-group">
                        <input type="password" class="form-control" id="input_password" placeholder="Пароль">
                      </div>
                      <div class="text-right mt-4"><a href="#" class="text-white">Забыли пароль?</a></div>
                      <a href="index.html"><button type="submit" class="btn btn-block btn-lg bg-white mt-4">Войти</button></a>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </main>
    <?php $this->load->view('footer'); ?>