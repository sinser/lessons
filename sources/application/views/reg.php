<?php $this->load->view('header'); ?>
<main class="main">
      <section class="py-xl bg-cover bg-size--cover" style="background-image: url('<?php echo site_url();?>resources/assets/images/backgrounds/img-1.jpg')">
        <span class="mask bg-primary alpha-6"></span>
        <div class="container d-flex align-items-center no-padding">
          <div class="col">
            <div class="row justify-content-center">
              <div class="col-lg-8">
                <div class="card bg-primary text-white">
                  <div class="card-body">
                    <button type="button" class="btn btn-primary btn-nobg btn-zoom--hover mb-5">
                      <a style="color: #fff; text-decoration: none;" href="<?php echo site_url();?>"><span class="btn-inner--icon"><i class="fas fa-arrow-left"></i></span></a>
                    </button>
                    <span class="clearfix"></span>
                    <img src="<?php echo site_url();?>resources/assets/images/brand/icon.png" style="width: 50px;">
                    <h4 class="heading h3 text-white pt-3 pb-5">Регистрация</h4>
                    <?php  
                            $attributes = array('method'=>'post', 'id' => 'reg-form', 'name' => 'reg-form'); 
                            echo form_open('Main/reg',$attributes);  
                    ?>
                      <div class="form-group">
                        <input type="text" class="form-control" id="name_org" placeholder="Наименование организации">
                      </div>
                      <div class="form-group">
                        <input type="text" class="form-control" id="sfera" placeholder="Сфера деятельности">
                      </div>
                      <div class="form-group">
                        <input type="text" class="form-control" id="main_services" placeholder="Основные услуги">
                      </div>
                      <div class="form-group">
                        <input type="text" class="form-control" id="contanct_face" placeholder="Контактное лицо">
                      </div>
                      <div class="form-group">
                        <input type="text" class="form-control" id="face_phone" placeholder="Номер телефона контактного лица">
                      </div>

                      <div class="form-group">
                        <input type="email" class="form-control" id="email" placeholder="Ваш email">
                      </div>
                      <div class="form-group">
                        <input type="password" class="form-control" id="password" placeholder="Пароль">
                      </div>
                      <div class="form-group">
                        <input type="password" class="form-control" id="spassword" placeholder="Повторите пароль">
                      </div>
                        <div class="custom-control custom-checkbox" style="margin-bottom: 20px;">
                        <input type="checkbox" class="custom-control-input" id="companyCheck">
                        <label class="custom-control-label" for="companyCheck">Я регистрируюсь как компания предоставляющая услуги</label>
                      </div>
                      <div class="text-right mt-4"><a href="<?php echo site_url();?>sign" class="text-white">Уже загрегистрированы?</a></div>
                      
                      <input type="button" class="btn btn-block btn-lg bg-white mt-4" id="regBtn" value="Зарегистрироваться">
                      <div id="error_box"></div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </main>
    <?php $this->load->view('footer'); ?>