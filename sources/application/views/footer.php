<footer class="pt-5 pb-3 footer  footer-dark bg-tertiary">
      <div class="container">
        <div class="row">
          <div class="col-12 col-md-4">
            <div class="pr-lg-5">
              <h1 class="heading h6 text-uppercase font-weight-700 mb-3"><strong>B2B</strong> Connect</h1>
              <p>Каталог компаний профессионалов. Выбирайте партнера по вашим критериям, отправляйте заявку и получите профессиональную консультацию и сервис.</p>
            </div>
          </div>
          <div class="col-6 col-md">
            <h5 class="heading h6 text-uppercase font-weight-700 mb-3">О системе</h5>
            <ul class="list-unstyled text-small">
              <li><a class="text-muted" href="#">Главная</a></li>
              <li><a class="text-muted" href="#">Каталог компаний</a></li>
              <li><a class="text-muted" href="#">Каталог услуг</a></li>
              <li><a class="text-muted" href="#">О системе</a></li>
              <li><a class="text-muted" href="#">Конткты</a></li>
            </ul>
          </div>
          <div class="col-6 col-md">
            <h5 class="heading h6 text-uppercase font-weight-700 mb-3">Личный кабинет</h5>
            <ul class="list-unstyled text-small">
              <li><a href="<?php echo site_url();?>sign"><button type="button" class="btn btn-sm btn-outline-primary btn-icon">
                    <span class="btn-inner--text">Авторизироваться</span>
                    <span class="btn-inner--icon"><svg class="svg-inline--fa fa-user fa-w-16" aria-hidden="true" data-prefix="fas" data-icon="user" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg=""><path fill="currentColor" d="M256 0c88.366 0 160 71.634 160 160s-71.634 160-160 160S96 248.366 96 160 167.634 0 256 0zm183.283 333.821l-71.313-17.828c-74.923 53.89-165.738 41.864-223.94 0l-71.313 17.828C29.981 344.505 0 382.903 0 426.955V464c0 26.51 21.49 48 48 48h416c26.51 0 48-21.49 48-48v-37.045c0-44.052-29.981-82.45-72.717-93.134z"></path></svg><!-- <i class="fas fa-user"></i> --></span>
                  </button></a>
              </li>
              <li><a href="<?php echo site_url();?>reg"><button type="button" class="btn btn-sm btn-outline-primary btn-icon">
                    <span class="btn-inner--text">Создать аккаунт компании</span>
                    <span class="btn-inner--icon"><svg class="svg-inline--fa fa-play fa-w-14" aria-hidden="true" data-prefix="fas" data-icon="play" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" data-fa-i2svg=""><path fill="currentColor" d="M424.4 214.7L72.4 6.6C43.8-10.3 0 6.1 0 47.9V464c0 37.5 40.7 60.1 72.4 41.3l352-208c31.4-18.5 31.5-64.1 0-82.6z"></path></svg><!-- <i class="fas fa-user"></i> --></span>
                  </button></a>
              </li>
            </ul>
          </div>
        </div>
        <hr>
      </div>
    </footer>
    <!-- Core -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <script src="<?php echo site_url();?>resources/assets/js/custom-scripts.js"></script>
    <script src="<?php echo site_url();?>resources/assets/vendor/jquery/jquery.min.js"></script>
    <script src="<?php echo site_url();?>resources/assets/vendor/popper/popper.min.js"></script>
    <script src="<?php echo site_url();?>resources/assets/js/bootstrap/bootstrap.min.js"></script>
    <!-- FontAwesome 5 -->
    <script src="<?php echo site_url();?>resources/assets/vendor/fontawesome/js/fontawesome-all.min.js" defer></script>
    <!-- Page plugins -->
    <script src="<?php echo site_url();?>resources/assets/vendor/bootstrap-select/js/bootstrap-select.min.js"></script>
    <script src="<?php echo site_url();?>resources/assets/vendor/bootstrap-tagsinput/bootstrap-tagsinput.min.js"></script>
    <script src="<?php echo site_url();?>resources/assets/vendor/input-mask/input-mask.min.js"></script>
    <script src="<?php echo site_url();?>resources/assets/vendor/nouislider/js/nouislider.min.js"></script>
    <script src="<?php echo site_url();?>resources/assets/vendor/textarea-autosize/textarea-autosize.min.js"></script>
    <!-- Theme JS -->
    <script src="<?php echo site_url();?>resources/assets/js/theme.js"></script>
  </body>
</html>