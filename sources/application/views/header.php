<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">

    <title>B2B Connect - Главная</title>
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:400,600,700,800|Roboto:400,500,700" rel="stylesheet">
    <!-- Theme CSS -->
    <link type="text/css" href="<?php echo site_url();?>resources/assets/css/theme.css" rel="stylesheet">
    <!-- Demo CSS - No need to use these in your project -->
    <link type="text/css" href="<?php echo site_url();?>resources/assets/css/demo.css" rel="stylesheet">
        <style type="text/css">
      .item-purchase-banner {
        display: none;
      }
    </style>
     <link type="text/css" href="<?php echo site_url();?>resources/assets/css/custom-styles.css" rel="stylesheet">
  </head>
  <body>
    <?php $current_page = $this->session->userdata('current_page'); ?>
    <input type="hidden" id="URL" value="<?php echo site_url();?>">
    <nav class="navbar navbar-expand-lg navbar-transparent navbar-light bg-white py-4">
      <div class="container">
        <a class="navbar-brand" style="padding: 10px; border-style: solid;" href="#"><strong>B2B</strong> Connect</a>
        <button class="navbar-toggler" type="button" data-action="offcanvas-open" data-target="#navbar_main" aria-controls="navbar_main" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="navbar-collapse offcanvas-collapse" id="navbar_main">
          <ul class="navbar-nav ml-auto align-items-lg-center">
            <h6 class="dropdown-header font-weight-600 d-lg-none px-0">Меню</h6>
            <li class="nav-item ">
              <a class="nav-link" href="<?php echo site_url();?>">Главная</a>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link" href="<?php echo site_url();?>catalogCompanies">Каталог компаний</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="<?php echo site_url();?>catalogServices">Каталог услуг</a>
            </li>
            <li class="nav-item">
              <a href="<?php echo site_url();?>reg"  class=" <?php if($current_page == "reg"){echo "active-item";}?>">
              <button type="button" class="btn btn-block btn-primary <?php if($current_page == "reg"){echo "active-item";}?>">Зарегистрироваться</button></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="<?php echo site_url();?>sign">
              <button type="button" class="btn btn-block btn-outline-success <?php if($current_page == "sign"){echo "active-item";}?>">Войти в личный кабинет</button></a>
            </li>
          </ul>
        </div>
      </div>
    </nav>