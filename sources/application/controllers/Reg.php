<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reg extends CI_Controller {

	public function index()
	{
        
        $newdata = array(
            'current_page'  => 'reg'
        );
        $this->session->set_userdata($newdata);
        
        $this->load->view('reg');
	}
}
