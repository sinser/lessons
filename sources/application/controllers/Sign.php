<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sign extends CI_Controller {

	public function index()
	{
        $newdata = array(
            'current_page'  => 'sign'
        );
        $this->session->set_userdata($newdata);

		$this->load->view('sign');
	}
}
